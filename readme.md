This application was built as a project within [this course](https://www.udemy.com/course/devslopes-ios10 "Devslopes Course") by Devslopes from Udemy. It is meant to be a companion app to the popular game [Pokémon Go](https://www.pokemongo.com/en-us/), a top game in both iOS and Android.

PokeSearch allows users to see pokémon sightings near their location, which have been submitted by other people. Users can also report their own sightings by choosing the pokémon they just saw in the game and locating it in the real map location, for other people to benefit.

[GeoFire](https://github.com/firebase/geofire-objc.git, "GeoFire Pod") was used to handle the pokémon location functionality, which stores location data on a Firebase database for convenience. 