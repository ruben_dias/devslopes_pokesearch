//
//  PokemonSearchCell.swift
//  PokeSearch
//
//  Created by Ruben Dias on 29/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import UIKit

class PokemonSearchCell: UITableViewCell, NibLoadableView {
    
    @IBOutlet weak var pokeImage: UIImageView!
    @IBOutlet weak var pokeNameLabel: UILabel!
    
    var pokemon: Int!
    
    func configureCell(pokemonID: Int, pokemonName: String) {
        self.pokemon = pokemonID
        self.pokeImage.image = UIImage(named: "\(pokemonID)")
        self.pokeNameLabel.text = pokemonName
    }
}
