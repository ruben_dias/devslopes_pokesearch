//
//  ViewController.swift
//  PokeSearch
//
//  Created by Ruben Dias on 28/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import FirebaseDatabase
import GeoFire

class MainVC: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, DataGeneratorDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popupView: UIView!
    
    
    var filteredPokeArray: Array<Pokemon> = []
    
    var inSearchMode = false
    
    let locationManager = CLLocationManager()
    var mapHasCenteredOnce = false
    var geoFire: GeoFire!
    var geoFireRef: DatabaseReference!
    
    var data: DataGenerator = DataGenerator.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data.delegate = self
        data.loadPokemons()
        
        mapView.delegate = self
        mapView.userTrackingMode = MKUserTrackingMode.follow
        
        geoFireRef = Database.database().reference()
        geoFire = GeoFire(firebaseRef: geoFireRef)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PokemonSearchCell.self)
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
    }
    
    func loadPokemons() {
        print("Pokémon Data loaded.")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationAuthStatus()
    }
    
    // GeoFire Location stuff
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            mapView.showsUserLocation = true
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
        
        mapView.setRegion(coordinateRegion, animated: true)
        mapHasCenteredOnce = true
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if let location = userLocation.location {
            if !mapHasCenteredOnce {
                centerMapOnLocation(location: location)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationIdentifier = "Pokemon"
        var annotationView: MKAnnotationView?
        
        if annotation.isKind(of: MKUserLocation.self) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "User")
            annotationView?.image = UIImage(named: "ash")
        } else if let dequeuedAnnotation = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotation
            annotationView?.annotation = annotation
        } else {
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView = av
        }
        
        if let annotationView = annotationView, let annotation = annotation as? PokeAnnotation {
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "\(annotation.pokemonID)")
            
            let button = UIButton()
            button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
            button.setImage(UIImage(named: "map"), for: .normal)
            annotationView.rightCalloutAccessoryView = button
        }
        
        return annotationView
    }
    
    func createSighting(forLocation location: CLLocation, withPokemon pokeID: Int) {
        geoFire.setLocation(location, forKey: "\(pokeID)")
        showSightingsOnMap(location: location)
    }
    
    func showSightingsOnMap(location: CLLocation) {
        let circleQuery = geoFire!.query(at: location, withRadius: 2.5)
        
        _ = circleQuery?.observe(GFEventType.keyEntered, with: { (key, location) in
            if let key = key, let location = location {
                let annotation = PokeAnnotation(coordinate: location.coordinate, pokemonID: Int(key)!)
                self.mapView.addAnnotation(annotation)
            }
        })
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let annotation = view.annotation as? PokeAnnotation {
            let place = MKPlacemark(coordinate: annotation.coordinate)
            let destination = MKMapItem(placemark: place)
            destination.name = "\(annotation.pokemonName)'s Location"
            let regionDistance: CLLocationDistance = 1000
            let regionSpan = MKCoordinateRegionMakeWithDistance(annotation.coordinate, regionDistance, regionDistance)
            
            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span), MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking] as [String : Any]
            
            MKMapItem.openMaps(with: [destination], launchOptions: options)
        }
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        showSightingsOnMap(location: location)
    }
    
    
    // Table View code
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(inSearchMode) {
            return filteredPokeArray.count
        } else {
            return data.pokeArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as PokemonSearchCell
        let poke: Pokemon!
        
        if(inSearchMode) {
            poke = filteredPokeArray[indexPath.row]
        } else {
            poke = data.pokeArray[indexPath.row]
        }
        cell.configureCell(pokemonID: poke.pokemonID, pokemonName: poke.pokemonName)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    // Search Bar code
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            
            tableView.reloadData()
        } else {
            inSearchMode = true
            
            filteredPokeArray = data.pokeArray.filter({$0.pokemonName.contains(find: searchBar.text!)})
            
            tableView.reloadData()
        }
    }
    
    // Deal with Popup View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view != popupView {
            dismisPopupView()
        }
    }
    
    // Storyboard Functionality
    
    @IBAction func showPopup(_ sender: Any) {
        if popupView.isHidden {
            popupView.isHidden = false
        } else {
            dismisPopupView()
        }
    }
    func dismisPopupView() {
        popupView.isHidden = true
        inSearchMode = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
    @IBAction func confirmSightingButton(_ sender: Any) {
        if let selectedCell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? PokemonSearchCell {
            let selectedPokeID = selectedCell.pokemon
            let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
            
            createSighting(forLocation: location, withPokemon: selectedPokeID!)

            dismisPopupView()
        }
    }
    
    // Random Pokemon
    //    @IBAction func spotRandomPokemon(_ sender: Any) {
    //        let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
    //        let randomPokemonID = arc4random_uniform(151) + 1
    //        createSighting(forLocation: location, withPokemon: Int(randomPokemonID))
    //    }
}

