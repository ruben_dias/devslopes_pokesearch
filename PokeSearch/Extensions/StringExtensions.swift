//
//  StringExtensions.swift
//  PokeSearch
//
//  Created by Ruben Dias on 30/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
