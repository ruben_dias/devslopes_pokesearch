//
//  NibLoadableView.swift
//  PokeSearch
//
//  Created by Ruben Dias on 29/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}
