//
//  ReusableView.swift
//  PokeSearch
//
//  Created by Ruben Dias on 29/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import UIKit

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
