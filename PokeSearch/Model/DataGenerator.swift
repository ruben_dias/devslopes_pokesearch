//
//  DataGenerator.swift
//  PokeSearch
//
//  Created by Ruben Dias on 29/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import Foundation

protocol DataGeneratorDelegate: class {
    func loadPokemons()
}

class DataGenerator {
    static let instance = DataGenerator()
    
    weak var delegate: DataGeneratorDelegate?
    
    var pokeArray: Array<Pokemon> = []
    
    func loadPokemons() {
        for index in 1...pokemon.count {
            pokeArray.append(Pokemon(pokemonID: index, pokemonName: pokemon[index - 1].capitalized))
        }
        
        delegate?.loadPokemons()
    }
}
