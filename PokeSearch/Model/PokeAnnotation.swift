//
//  PokeAnnotation.swift
//  PokeSearch
//
//  Created by Ruben Dias on 29/12/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import Foundation
import MapKit

class PokeAnnotation: NSObject, MKAnnotation {
    var coordinate = CLLocationCoordinate2D ()
    var pokemonID: Int
    var pokemonName: String
    var title: String?
    
    init(coordinate: CLLocationCoordinate2D, pokemonID: Int) {
        self.coordinate = coordinate
        self.pokemonID = pokemonID
        self.pokemonName = pokemon[pokemonID - 1].capitalized
        self.title = self.pokemonName
    }
}




